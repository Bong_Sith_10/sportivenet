package com.example.nhoeksith.myapp.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.nhoeksith.myapp.R;

public class InputCode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_code);
    }

    public void OnClickSignUp(View view) {
        Intent intent=new Intent(this,PasswordActivity.class);
        startActivity(intent);
        finish();

    }

    public void BackToPhone(View view) {
        Intent intent=new Intent(this,PhoneNumber.class);
        startActivity(intent);
        finish();

    }

    public void BackToInputCode(View view) {
        Intent intent=new Intent(this,InputCode.class);
        startActivity(intent);
    }
}
