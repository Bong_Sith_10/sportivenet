package com.example.nhoeksith.myapp.SearchTeam;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.nhoeksith.myapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class AdapterTeam extends RecyclerView.Adapter<AdapterTeam.ViewHolder> implements Filterable  {
    private ArrayList<ItemTeam> mArrayListField;
    private ArrayList<ItemTeam> mFilteredList;

    private Context mContext;

    public AdapterTeam(ArrayList<ItemTeam> arrayList,Context context) {
        this.mArrayListField=arrayList;
        this.mFilteredList=arrayList;

        this.mContext=context;
    }

    @Override
    public AdapterTeam.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_search_team,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterTeam.ViewHolder holder, int i) {
        ItemTeam itemTeam=mArrayListField.get(i);
        holder.tv_name_Team.setText(itemTeam.getName_field());
        holder.tv_name_Capitain.setText(itemTeam.getName_capitain());
        Picasso.with(mContext)
                .load(mArrayListField.get(i).getMglogoTeam())
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.MglogoTeam);

    }

    @Override
    public int getItemCount() {
        return mArrayListField.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayListField;
                } else {

                    ArrayList<ItemTeam> filteredList = new ArrayList<>();

                    for (ItemTeam androidVersion : mArrayListField) {

                        if (androidVersion.getApi().toLowerCase().contains(charString) || androidVersion.getName_field().toLowerCase().contains(charString) || androidVersion.getName_capitain().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ItemTeam>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name_Team, tv_name_Capitain;
        private CardView cv;
        private CircleImageView MglogoTeam;

        public ViewHolder(View view) {
            super(view);
           // cv= (CardView) view.findViewById(R.id.id_cv_searchTeam);
            tv_name_Team = (TextView) view.findViewById(R.id.id_tv_teamName);
            tv_name_Capitain = (TextView) view.findViewById(R.id.id_tv_nameCapitain);
            MglogoTeam=(CircleImageView) view.findViewById(R.id.id_MgLogo_team);


        }
    }
}
