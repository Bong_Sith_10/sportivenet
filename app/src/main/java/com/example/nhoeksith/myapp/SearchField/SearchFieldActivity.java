package com.example.nhoeksith.myapp.SearchField;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nhoeksith.myapp.News.AppController;
import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.help.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchFieldActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private ArrayList<ItemField> mArrayList=new ArrayList<>();
    private AdapterField mAdapter;
    public String[] listFieldName={"Down Town","Pnom Penh","Footbal Live","Smart Club","Phum Yeurng","Cold Clup","Nice Club","Big Man Club","Foreing Club","Man FC","Happy Club"};
    public String[] listLogoField={"http://pictures.replayphotos.com/images/UAL/smd/university-of-alabama-football-automatically-imported-western-kentucky-v-alabama-ual-f-auto-00072smd.jpg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXh67gqE-sH7xNqPm26_EZZXdgmyJl8BgeSp4S3BADF-q-r9Du",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS72LlpukWVMY1LBgOLEzuJP_nGy9ixRC236K0aZccIUP7B38PD",
            "http://pictures.replayphotos.com/images/UAL/smd/university-of-alabama-football-automatically-imported-western-kentucky-v-alabama-ual-f-auto-00072smd.jpg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXh67gqE-sH7xNqPm26_EZZXdgmyJl8BgeSp4S3BADF-q-r9Du"
            ,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS72LlpukWVMY1LBgOLEzuJP_nGy9ixRC236K0aZccIUP7B38PD"};
    EditText search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_field);

        search=(EditText)findViewById(R.id.id_serach_field);
        recyclerView=(RecyclerView)findViewById(R.id.id_rcv_field);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter=new AdapterField(mArrayList,this);
        recyclerView.setAdapter(mAdapter);

        RequestData();
        addTextListener();

    }


    public void backSearchField(View view) {
        finish();
    }

    public void RequestData() {
        // progressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://104.131.34.16:8080/spnet_api/fields.json";
        StringRequest JOR = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Log.d(TAG, response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("fields");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                JSONObject jsonObject2 = jsonObject1.getJSONObject("Field");

                                String image = jsonObject2.getString("logo_image_url");
                                String title = jsonObject2.getString("name");
                                String phone = jsonObject2.getString("phone_line_1");
                                String id = jsonObject2.getString("id");

                                mArrayList.add(new ItemField(title,image,phone));
                                Log.d("test", title);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("secret_key", "123");
                return headers;
            }
        };
        MySingleton.getInstance(getApplication()).addToRequestQueue(JOR);
        //page=page+1;
    }

    public void addTextListener(){

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<ItemField> filteredList = new ArrayList<ItemField>();

                for (int i = 0; i < mArrayList.size(); i++) {

                    final String text = mArrayList.get(i).getName_field().toLowerCase();
                    if (text.contains(query)) {


                        filteredList.add(new ItemField(mArrayList.get(i).getName_field(),mArrayList.get(i).getMg_field(),mArrayList.get(i).getField_phone()));
                    }
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(getApplication()));
                mAdapter = new AdapterField((ArrayList<ItemField>) filteredList,getApplication());
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();  // data set changed
            }
        });
    }
}
