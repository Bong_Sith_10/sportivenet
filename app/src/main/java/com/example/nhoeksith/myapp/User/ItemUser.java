package com.example.nhoeksith.myapp.User;

import java.io.Serializable;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class ItemUser implements Serializable {

    public String name_user;
    public String mg_user;
    public String api;

    public ItemUser(String name_user, String mg_user) {
        this.name_user = name_user;
        this.mg_user = mg_user;
        //this.api = api;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getMg_user() {
        return mg_user;
    }

    public void setMg_user(String mg_user) {
        this.mg_user = mg_user;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }
}
