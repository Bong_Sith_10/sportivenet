package com.example.nhoeksith.myapp.Slide_Adater_field;

/**
 * Created by NHOEK Sith on 5/25/2017.
 */

public class ItemFieldSlide {
    private String mg_field;
    private String name_field;
    private String phone_field;
    private String price_field;
    private String address_field;

    public ItemFieldSlide(String mg_field, String name_field, String phone_field, String price_field, String address_field) {
        this.mg_field = mg_field;
        this.name_field = name_field;
        this.phone_field = phone_field;
        this.price_field = price_field;
        this.address_field = address_field;
    }

    public String getMg_field() {
        return mg_field;
    }

    public void setMg_field(String mg_field) {
        this.mg_field = mg_field;
    }

    public String getName_field() {
        return name_field;
    }

    public void setName_field(String name_field) {
        this.name_field = name_field;
    }

    public String getPhone_field() {
        return phone_field;
    }

    public void setPhone_field(String phone_field) {
        this.phone_field = phone_field;
    }

    public String getPrice_field() {
        return price_field;
    }

    public void setPrice_field(String price_field) {
        this.price_field = price_field;
    }

    public String getAddress_field() {
        return address_field;
    }

    public void setAddress_field(String address_field) {
        this.address_field = address_field;
    }

}
