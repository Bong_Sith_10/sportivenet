package com.example.nhoeksith.myapp.CreateMatch;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.SearchField.SearchFieldActivity;
import com.example.nhoeksith.myapp.SearchTeam.SearchTeamActivity;
import com.example.nhoeksith.myapp.User.AddMemerActivity;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by NHOEK Sith on 4/10/2017.
 */

public class GroupeMatchFragment extends Fragment {
    private TextView mLenght,mDate,mTime,mField;
    private CircleImageView circleImageView;

    Calendar mDates= Calendar.getInstance();
    String hour="00",minutes="00";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.groupe_match_fragment,container,false);

        mLenght= (TextView) view.findViewById(R.id.id_tv_lGM);
        mDate= (TextView) view.findViewById(R.id.id_date_GM);
        mTime= (TextView) view.findViewById(R.id.id_time_GM);
        mField= (TextView) view.findViewById(R.id.id_field_GM);
        circleImageView=(CircleImageView)view.findViewById(R.id.id_add_member);


        updateDateDisplay();
        updateTimeDisplay();
        updateLengthDisplay();

        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.OnDateSetListener mDateListener = new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        mDates.set(java.util.Calendar.YEAR, year);
                        mDates.set(java.util.Calendar.MONTH, monthOfYear);
                        mDates.set(java.util.Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateDateDisplay();
                    }
                };

                new DatePickerDialog(getActivity(), mDateListener,
                        mDates.get(java.util.Calendar.YEAR),
                        mDates.get(java.util.Calendar.MONTH),
                        mDates.get(java.util.Calendar.DAY_OF_MONTH)).show();

            }

        });

        mTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.OnTimeSetListener mTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mDates.set(java.util.Calendar.HOUR_OF_DAY, hourOfDay);
                        mDates.set(java.util.Calendar.MINUTE, minute);
                        updateTimeDisplay();
                    }
                };

                new TimePickerDialog(getActivity(), mTimeListener,
                        mDates.get(java.util.Calendar.HOUR_OF_DAY),
                        mDates.get(java.util.Calendar.MINUTE), true).show();
            }
        });

        mLenght.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.OnTimeSetListener mTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mDates.set(java.util.Calendar.HOUR_OF_DAY, hourOfDay);
                        mDates.set(java.util.Calendar.MINUTE, minute);
                        hour=String.valueOf(hourOfDay);
                        minutes=String.valueOf(minute);
                        updateLengthDisplay();
                    }
                };

                new TimePickerDialog(getActivity(), mTimeListener,
                        mDates.get(java.util.Calendar.HOUR_OF_DAY),
                        mDates.get(java.util.Calendar.MINUTE), true).show();
            }


        });

        mField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), SearchFieldActivity.class);
                startActivity(intent);

            }
        });



        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), AddMemerActivity.class);
                startActivity(intent);

            }
        });

        return view;
    }

    private void updateDateDisplay() {
        mDate.setText(DateUtils.formatDateTime(getActivity(),
                mDates.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE));
    }
    private void updateTimeDisplay() {
        mTime.setText(DateUtils.formatDateTime(getActivity(),
                mDates.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME));
    }
    private void updateLengthDisplay() {
        mLenght.setText(hour+"h"+minutes+"mn");
    }
}
