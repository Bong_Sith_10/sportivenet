package com.example.nhoeksith.myapp.CreateMatch;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.SearchField.SearchFieldActivity;
import com.example.nhoeksith.myapp.SearchTeam.SearchTeamActivity;

/**
 * Created by NHOEK Sith on 4/10/2017.
 */

public class TeamMatchFragment extends Fragment {
    TextView mTextDate,mTextTime,mTextLength,mTextField;
    ImageView mMgSt;
    java.util.Calendar mDate= java.util.Calendar.getInstance();
    String hour="00",minutes="00";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.team_match_fragment,container,false);
        mTextDate= (TextView) view.findViewById(R.id.id_date_crtm);
        mTextTime= (TextView) view.findViewById(R.id.id_time_ctm);
        mTextLength= (TextView) view.findViewById(R.id.id_length_crtm);
        mTextField= (TextView) view.findViewById(R.id.id_field_crtm);
        mMgSt= (ImageView) view.findViewById(R.id.id_select_team);

        updateDateDisplay();
        updateTimeDisplay();
        updateLengthDisplay();
        mTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.OnDateSetListener mDateListener = new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        mDate.set(java.util.Calendar.YEAR, year);
                        mDate.set(java.util.Calendar.MONTH, monthOfYear);
                        mDate.set(java.util.Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateDateDisplay();
                    }
                };

                new DatePickerDialog(getActivity(), mDateListener,
                        mDate.get(java.util.Calendar.YEAR),
                        mDate.get(java.util.Calendar.MONTH),
                        mDate.get(java.util.Calendar.DAY_OF_MONTH)).show();

            }

        });

        mTextTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.OnTimeSetListener mTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mDate.set(java.util.Calendar.HOUR_OF_DAY, hourOfDay);
                        mDate.set(java.util.Calendar.MINUTE, minute);
                        updateTimeDisplay();
                    }
                };

                new TimePickerDialog(getActivity(), mTimeListener,
                        mDate.get(java.util.Calendar.HOUR_OF_DAY),
                        mDate.get(java.util.Calendar.MINUTE), true).show();
            }
        });

        mTextLength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog.OnTimeSetListener mTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mDate.set(java.util.Calendar.HOUR_OF_DAY, hourOfDay);
                        mDate.set(java.util.Calendar.MINUTE, minute);
                        hour=String.valueOf(hourOfDay);
                        minutes=String.valueOf(minute);
                        updateLengthDisplay();
                    }
                };

                new TimePickerDialog(getActivity(), mTimeListener,
                        mDate.get(java.util.Calendar.HOUR_OF_DAY),
                        mDate.get(java.util.Calendar.MINUTE), true).show();
            }


        });

        mTextField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), SearchFieldActivity.class);
                startActivity(intent);

            }
        });


        mMgSt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), SearchTeamActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    private void updateDateDisplay() {
        mTextDate.setText(DateUtils.formatDateTime(getActivity(),
                mDate.getTimeInMillis(), DateUtils.FORMAT_SHOW_DATE));
    }
    private void updateTimeDisplay() {
        mTextTime.setText(DateUtils.formatDateTime(getActivity(),
                mDate.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME));
    }
    private void updateLengthDisplay() {
        mTextLength.setText(hour+"h"+minutes+"mn");
    }

}
