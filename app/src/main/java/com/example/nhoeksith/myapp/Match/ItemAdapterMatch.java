package com.example.nhoeksith.myapp.Match;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nhoeksith.myapp.Activity.DetailMatchActivity;
import com.example.nhoeksith.myapp.CreateMatch.CreateMatchActivity;
import com.example.nhoeksith.myapp.News.Item;
import com.example.nhoeksith.myapp.R;

import java.util.ArrayList;

/**
 * Created by NHOEK Sith on 4/3/2017.
 */

public class ItemAdapterMatch extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private ArrayList<Item> mItemList;
     Context mContext;

    final int VIEW_0=0;
    final int VIEW_1=1;
    final int VIEW_2=2;
    final int VIEW_3=3;
    final int VIEW_4=4;
    final int VIEW_5=5;
    final int VIEW_6=5;

    public ItemAdapterMatch(ArrayList<Item> itemList, Context context) {
        this.mItemList=itemList;
        this.mContext=context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0){
            return VIEW_0;
        }else if (position==1){
            return VIEW_1;
        }else if (position==2){
            return VIEW_2;
        }else if (position==3){
            return VIEW_3;
        }else if (position==4){
            return VIEW_4;
        }else if (position==5){
            return VIEW_5;
        }else {
            return VIEW_6;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view;
        RecyclerView.ViewHolder holder=null;
        if (viewType==0){
            view=inflater.inflate(R.layout.item_cardview_mathch_team,parent,false);
            holder=new ItemViewHolder0(view);

        }else if (viewType==1){
            view = inflater.inflate(R.layout.item_cardview_create_match, parent, false);
            holder=new ItemViewHolder1(view);
        }
        else if (viewType==2){
            view = inflater.inflate(R.layout.item_cardview_request_match, parent, false);
            holder=new ItemViewHolder2(view);
        }
        else if (viewType==3){
            view = inflater.inflate(R.layout.item_cardview_invite_tojoin_team, parent, false);
            holder=new ItemViewHolder3(view);
        }
        else if (viewType==4){
            view = inflater.inflate(R.layout.item_cardview_grouge_match, parent, false);
            holder=new ItemViewHolder4(view);
        }else if (viewType==5){
            view = inflater.inflate(R.layout.item_cardview_groupe_invite, parent, false);
            holder=new ItemViewHolder5(view);
        }
        else {
            view = inflater.inflate(R.layout.item_cardview_team_find_competitor, parent, false);
            holder=new ItemViewHolder6(view);
        }
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder0){
            ItemViewHolder0 holder0= (ItemViewHolder0) holder;
            //holder1.tvDesc.setText(mItemList.get(position-1).getTittle());
//            Picasso.with(mContext)
//                    .load(mItemList.get(position).image1)
//                    .placeholder(android.R.drawable.ic_menu_upload_you_tube)
//                    .error(android.R.drawable.stat_notify_error)
//                    .into(holder0.ivImage);
        }
        else if (holder instanceof ItemViewHolder1){
            ItemViewHolder1 holder1= (ItemViewHolder1) holder;
//            holder1.tvDesc.setText(mItemList.get(position-1).title);
//            Picasso.with(mContext)
//                    .load(mItemList.get(position-1).image)
//                    .placeholder(android.R.drawable.ic_menu_upload_you_tube)
//                    .error(android.R.drawable.stat_notify_error)
//                    .into(holder1.ivImage);

        }else if (holder instanceof ItemViewHolder2){
            ItemViewHolder2 holder2= (ItemViewHolder2) holder;
        }
        else if (holder instanceof ItemViewHolder3){
            ItemViewHolder3 holder3= (ItemViewHolder3) holder;
        }
        else if (holder instanceof ItemViewHolder4){
            ItemViewHolder4 holder4= (ItemViewHolder4) holder;
        }
        else if (holder instanceof ItemViewHolder5){
            ItemViewHolder5 holder5= (ItemViewHolder5) holder;
        }
        else {
            ItemViewHolder6 holder6= (ItemViewHolder6) holder;
        }
//        Item item=mItemList.get(position);
//        Picasso.with(mContext)
//                .load(item.image)
//                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
//                .error(android.R.drawable.stat_notify_error)
//                .into();


    }

    @Override
    public int getItemCount() {
        return 7;
    }

    public static class ItemViewHolder0 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView cvItem;
        public ImageView ivImage;
        Context context;

        public ItemViewHolder0(View itemView) {
            super(itemView);
            cvItem=(CardView) itemView.findViewById(R.id.cv_teamMatch_id);
//            ivImage=(ImageView)itemView.findViewById(R.id.im_new_id);
//            itemView.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Context context=itemView.getContext();
            Intent intent=new Intent(context, DetailMatchActivity.class);
            context.startActivity(intent);

        }
    }
    public static class ItemViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView cvItem;
        public ImageView ivImage;
        public TextView tvDesc;
        public Button button;

        public ItemViewHolder1(View itemView) {
            super(itemView);


            cvItem = (CardView) itemView.findViewById(R.id.cardview_CreateMatch_id);

//            ivImage=(ImageView)itemView.findViewById(R.id.im2_new_id);
//            tvDesc=(TextView) itemView.findViewById(R.id.id_text_card2);
            button= (Button) itemView.findViewById(R.id.id_bt_createMatch);
            button.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            Context context=button.getContext();
            Intent intent=new Intent(context, CreateMatchActivity.class);
            context.startActivity(intent);

        }
    }

        public static class ItemViewHolder2 extends RecyclerView.ViewHolder implements View.OnClickListener {
            public CardView cvItem;
            public ImageView ivImage;
            public TextView tvDesc;

            public ItemViewHolder2(View itemView) {
                super(itemView);


                cvItem = (CardView) itemView.findViewById(R.id.cardview_RequestMatch_id);
//            ivImage=(ImageView)itemView.findViewById(R.id.im2_new_id);
//            tvDesc=(TextView) itemView.findViewById(R.id.id_text_card2);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                Context context=itemView.getContext();
                Intent intent=new Intent(context,DetailMatchActivity.class);
                context.startActivity(intent);
            }
        }

        public static class ItemViewHolder3 extends RecyclerView.ViewHolder implements View.OnClickListener {
            public CardView cvItem;
            public ImageView ivImage;
            public TextView tvDesc;

            public ItemViewHolder3(View itemView) {
                super(itemView);


                cvItem = (CardView) itemView.findViewById(R.id.cv_inviteToJoinTeam_id);
//            ivImage=(ImageView)itemView.findViewById(R.id.im2_new_id);
//            tvDesc=(TextView) itemView.findViewById(R.id.id_text_card2);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                Context context=itemView.getContext();
                Intent intent=new Intent(context,DetailMatchActivity.class);
                context.startActivity(intent);
            }
        }
        public static class ItemViewHolder4 extends RecyclerView.ViewHolder implements View.OnClickListener {
            public CardView cvItem;
            public ImageView ivImage;
            public TextView tvDesc;

            public ItemViewHolder4(View itemView) {
                super(itemView);


                cvItem = (CardView) itemView.findViewById(R.id.cv_groupeMatch_id);
//            ivImage=(ImageView)itemView.findViewById(R.id.im2_new_id);
//            tvDesc=(TextView) itemView.findViewById(R.id.id_text_card2);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                Context context=itemView.getContext();
                Intent intent=new Intent(context,DetailMatchActivity.class);
                context.startActivity(intent);
            }
        }
        public static class ItemViewHolder5 extends RecyclerView.ViewHolder implements View.OnClickListener {
            public CardView cvItem;
            public ImageView ivImage;
            public TextView tvDesc;

            public ItemViewHolder5(View itemView) {
                super(itemView);


                cvItem = (CardView) itemView.findViewById(R.id.cv_grogeInvite_id);
//            ivImage=(ImageView)itemView.findViewById(R.id.im2_new_id);
//            tvDesc=(TextView) itemView.findViewById(R.id.id_text_card2);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                Context context=itemView.getContext();
                Intent intent=new Intent(context,DetailMatchActivity.class);
                context.startActivity(intent);
            }
        }
        public static class ItemViewHolder6 extends RecyclerView.ViewHolder implements View.OnClickListener {
            public CardView cvItem;
            public ImageView ivImage;
            public TextView tvDesc;

            public ItemViewHolder6(View itemView) {
                super(itemView);


                cvItem = (CardView) itemView.findViewById(R.id.cardview_findCompetitor_id);
//            ivImage=(ImageView)itemView.findViewById(R.id.im2_new_id);
//            tvDesc=(TextView) itemView.findViewById(R.id.id_text_card2);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                Context context=itemView.getContext();
                Intent intent=new Intent(context,DetailMatchActivity.class);
                context.startActivity(intent);
            }
        }


    }

