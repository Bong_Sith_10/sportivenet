package com.example.nhoeksith.myapp.Slide_Adater_field;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nhoeksith.myapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.nhoeksith.myapp.R.layout.slide_field_row;

/**
 * Created by NHOEK Sith on 5/25/2017.
 */

public class AdapterFieldSlide extends ArrayAdapter<ItemFieldSlide> {
    ArrayList<ItemFieldSlide> arrayList;

    public AdapterFieldSlide(Context context, ArrayList<ItemFieldSlide> arrayList) {
        super(context, R.layout.slide_field_row,arrayList);
        this.arrayList = arrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }
    private View initView(int position,View convertView,ViewGroup parent){
        ItemFieldSlide itemFieldSlide= (ItemFieldSlide) getItem(position);
        if (convertView==null) {
            if (parent == null)
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.slide_field_row, null);
            else
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.slide_field_row, parent, false);
        }
        ImageView logo_field=(ImageView)convertView.findViewById(R.id.id_mg_slideField);
        TextView name_field=(TextView)convertView.findViewById(R.id.id_name_fieldSlide);

        if (logo_field!=null){
            Picasso.with(getContext())
                    .load(itemFieldSlide.getMg_field())
                    .into(logo_field);
        }
        if (name_field!=null){
            name_field.setText(itemFieldSlide.getName_field());
        }
        return convertView;
    }
}
