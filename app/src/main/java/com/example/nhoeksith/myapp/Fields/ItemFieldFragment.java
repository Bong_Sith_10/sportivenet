package com.example.nhoeksith.myapp.Fields;

import java.io.Serializable;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class ItemFieldFragment implements Serializable {

    private String name_field;
    private String mg_field;
    private String phone;
    public int id;
    private double longtitute,lattitute;


    public ItemFieldFragment(String name_field, String mg_field,String phone,double longtitute,double lattitute,int id) {

        this.longtitute=longtitute;
        this.lattitute=lattitute;
        this.name_field = name_field;
        this.mg_field = mg_field;
        this.phone=phone;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLongtitute() {
        return longtitute;
    }

    public void setLongtitute(double longtitute) {
        this.longtitute = longtitute;
    }

    public double getLattitute() {
        return lattitute;
    }

    public void setLattitute(double lattitute) {
        this.lattitute = lattitute;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName_field() {
        return name_field;
    }

    public void setName_field(String name_field) {
        this.name_field = name_field;
    }

    public String getMg_field() {
        return mg_field;
    }

    public void setMg_field(String mg_field) {
        this.mg_field = mg_field;
    }


}
