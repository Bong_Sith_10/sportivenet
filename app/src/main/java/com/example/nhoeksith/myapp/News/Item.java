package com.example.nhoeksith.myapp.News;

import java.io.Serializable;

/**
 * Created by NHOEK Sith on 4/3/2017.
 */

public class Item implements Serializable {
    public String image1="http://www4.pictures.zimbio.com/gi/Liverpool+v+Cardiff+City+Carling+Cup+Final+lto8hFciK7Gl.jpg";
    public String image;
    public String title;
    private String url;
    private String created_at;

    Item(String image, String title, String url, String created_at) {
        this.image = image;
        this.title = title;
        this.url = url;
        this.created_at=created_at;
    }

    String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
