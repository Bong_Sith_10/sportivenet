package com.example.nhoeksith.myapp.Navigation;

import java.io.Serializable;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class ItemTeamNav implements Serializable {
    public String mglogoTeam;
    public String name_team;
    public String api;

    public ItemTeamNav(String name_team , String mglogoTeam) {
        this.name_team = name_team;
        this.mglogoTeam=mglogoTeam;
    }

    public String getMglogoTeam() {
        return mglogoTeam;
    }

    public void setMglogoTeam(String mglogoTeam) {
        this.mglogoTeam = mglogoTeam;
    }

    public String getName_team() {
        return name_team;
    }

    public void setName_team(String name_team) {
        this.name_team = name_team;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }
}
