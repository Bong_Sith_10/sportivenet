package com.example.nhoeksith.myapp.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nhoeksith.myapp.Activity.HomeActivity;
import com.example.nhoeksith.myapp.Activity.LoginActivity;
import com.example.nhoeksith.myapp.R;

/**
 * Created by NHOEK Sith on 5/19/2017.
 */

public class SecondMatchFragment extends android.support.v4.app.Fragment {
    private static final int DIALOG_LOGIN = 1;

    Button login,btn_login;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.second_match_fragement,container,false);
        login=(Button)view.findViewById(R.id.id_bt_login);


        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                FragmentManager fragmentManager=getFragmentManager();
//                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
//                MatchFragment matchFragment=new MatchFragment();
//                fragmentTransaction.replace(R.id.id_layout_match,matchFragment);
//                fragmentTransaction.commit();

               // onPrepareDialog(DIALOG_LOGIN,onCreateDialog(DIALOG_LOGIN));

                Intent intent=new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });


        return view;
    }



}
