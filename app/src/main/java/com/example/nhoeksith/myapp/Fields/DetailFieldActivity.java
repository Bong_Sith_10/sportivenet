package com.example.nhoeksith.myapp.Fields;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nhoeksith.myapp.News.AppController;
import com.example.nhoeksith.myapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class DetailFieldActivity extends AppCompatActivity implements OnMapReadyCallback {
    final private static int DIALOG_PHONE = 1;
    MapView mapView;
    GoogleMap map, map1;
    private ImageView mg_logo;
    private TextView name_field;
    private Button button_call;
    private String phone_number;
    private int id;
    Button phone1, phone2, phone3, phone4;
    double longitude;
    double latitude;
    int la;
    int lo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_field);
        mg_logo = (ImageView) findViewById(R.id.id_detaiF_logo);
        name_field = (TextView) findViewById(R.id.id_tvDetail_FieldName);
        mapView = (MapView) findViewById(R.id.id_maps);


        // phone1=(TextView) findViewById(R.id.id_phone1);


        //phone_number= (String) button_call.getText();
        button_call = (Button) findViewById(R.id.id_bnt_call);

        Picasso.with(this)
                .load(getIntent().getStringExtra("LogoField"))
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(mg_logo);
        name_field.setText(getIntent().getStringExtra("NameField"));
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        id = getIntent().getIntExtra("id", 0);





    }

    public void OnClickbackFieldDetail(View view) {
        finish();
    }

    public void OnClickCall(View view) {
//        phone_number = button_call.getText().toString();
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:" + phone_number));
//        startActivity(intent);

        showDialog(DIALOG_PHONE);
        RequestData();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog dialogDetails = null;
        switch (id) {

            case DIALOG_PHONE:
                LayoutInflater inflater = LayoutInflater.from(this);
                View dialogview = inflater.inflate(R.layout.dialog_phone, null);
                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
                // dialogbuilder.setTitle("longin");
                dialogbuilder.setView(dialogview);
                dialogDetails = dialogbuilder.create();
                break;

        }

        return dialogDetails;
    }

    @Override

    protected void onPrepareDialog(int id, Dialog dialog) {


        switch (id) {

            case DIALOG_PHONE:
                final AlertDialog alertDialog = (AlertDialog) dialog;
                phone1 = (Button) alertDialog.findViewById(R.id.id_phone1);
                phone2=(Button)alertDialog.findViewById(R.id.id_phone2);
                phone3=(Button)alertDialog.findViewById(R.id.id_phone3);
                phone4=(Button)alertDialog.findViewById(R.id.id_phone4);
                phone1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        phone_number=phone1.getText().toString();
                        if (phone_number!="null") {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + phone_number));
                            startActivity(intent);
                        }

                    }
                });
                phone2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        phone_number = phone2.getText().toString();
                        if (phone_number != "null") {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + phone_number));
                            startActivity(intent);
                        }


                    }
                });
                phone3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        phone_number=phone3.getText().toString();
                        if (phone_number!="null") {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + phone_number));
                            startActivity(intent);
                        }

                    }
                });
                phone4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        phone_number=phone4.getText().toString();
                        if (phone_number!="null") {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + phone_number));
                            startActivity(intent);
                        }

                    }
                });


                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        map1=googleMap;
        map.getUiSettings().setZoomControlsEnabled(true);
        map.addMarker(new MarkerOptions().position(new LatLng(getIntent().getDoubleExtra("longt", (float) 11.5407637),getIntent().getDoubleExtra("latt",(double) 104.8738575))).title(getIntent().getStringExtra("NameField")));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(getIntent().getDoubleExtra("longt", (float) 11.5407637),getIntent().getDoubleExtra("latt",(double) 104.8738575)),15));


//        map1.getUiSettings().setZoomControlsEnabled(true);
//        map1.addMarker(new MarkerOptions().position(new LatLng(longitude,latitude)).title("Hello").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
//        map1.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(longitude,latitude),15));

    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    public void RequestData() {
        // progressDialog.show();
        String tag_string_req = "req_register";
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://104.131.34.16:8080/spnet_api/fields.json";
        StringRequest JOR = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("fields");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                JSONObject jsonObject2 = jsonObject1.getJSONObject("Field");
                                //int ids = int.jsonObject2.getString("id");
                                int ids=Integer.parseInt(jsonObject2.getString("id"));
                                if (id==ids) {
                                    phone1.setText(jsonObject2.getString("phone_line_1"));
                                    phone2.setText(jsonObject2.getString("phone_line_2"));
                                    phone3.setText(jsonObject2.getString("phone_line_3"));
                                    phone4.setText(jsonObject2.getString("phone_line_4"));

                                }


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("secret_key", "123");
                return headers;
            }
        };
        queue.add(JOR);
        //AppController.getInstance().addToRequestQueue(JOR);

        //page=page+1;
    }


}
