package com.example.nhoeksith.myapp.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterViewFlipper;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nhoeksith.myapp.Fields.AdapterFieldFragment;
import com.example.nhoeksith.myapp.Fields.ItemFieldFragment;
import com.example.nhoeksith.myapp.News.AppController;
import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.Slide_Adater_field.AdapterFieldSlide;
import com.example.nhoeksith.myapp.Slide_Adater_field.ItemFieldSlide;
import com.example.nhoeksith.myapp.help.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by NHOEK Sith on 3/17/2017.
 */

public class FieldFragment extends android.support.v4.app.Fragment {
    EditText search;

    AdapterViewFlipper adapterViewFlipper;
    ArrayList<ItemFieldSlide> slideLsist;
    ImageView imageSlide;

    RecyclerView recyclerView;
    private ArrayList<ItemFieldFragment> mArrayList=new ArrayList<>();
    private AdapterFieldFragment mAdapter;
    private String[] listFieldName={"Down Town","Pnom Penh","Footbal Live","Smart Club","Phum Yeurng","Cold Clup","Nice Club","Big Man Club","Foreing Club","Man FC","Happy Club"};
    private String[] listLogoField={"http://pictures.replayphotos.com/images/UAL/smd/university-of-alabama-football-automatically-imported-western-kentucky-v-alabama-ual-f-auto-00072smd.jpg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXh67gqE-sH7xNqPm26_EZZXdgmyJl8BgeSp4S3BADF-q-r9Du",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS72LlpukWVMY1LBgOLEzuJP_nGy9ixRC236K0aZccIUP7B38PD",
            "http://pictures.replayphotos.com/images/UAL/smd/university-of-alabama-football-automatically-imported-western-kentucky-v-alabama-ual-f-auto-00072smd.jpg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXh67gqE-sH7xNqPm26_EZZXdgmyJl8BgeSp4S3BADF-q-r9Du"
            ,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS72LlpukWVMY1LBgOLEzuJP_nGy9ixRC236K0aZccIUP7B38PD"};
    SearchView searchView;

    ProgressDialog dialog;



    public FieldFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.field_fragment,container,false);

        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Wait while loading...");
        dialog.isIndeterminate();
        dialog.setCancelable(true);

        RequestData();

        adapterViewFlipper=(AdapterViewFlipper)view.findViewById(R.id.id_adapter_fliper);
      //  imageSlide=(ImageView)adapterViewFlipper.findViewById(R.id.id_mg_fieldSlide);
        slideLsist=new ArrayList<ItemFieldSlide>();
        slideLsist=slideFieldReques(slideLsist);
        AdapterFieldSlide adapterFieldSlide=new AdapterFieldSlide(getActivity(),slideLsist);
        adapterViewFlipper.setAdapter(adapterFieldSlide);


        recyclerView=(RecyclerView)view.findViewById(R.id.id_rc_field_fragment);
       // searchView=(SearchView)view.findViewById(R.id.id_search_field);
       // search=(EditText)view.findViewById(R.id.id_search_txt);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter=new AdapterFieldFragment(mArrayList,getActivity());
        recyclerView.setAdapter(mAdapter);


     // addTextListener();


    //    getData();


        return view;
    }


    public void RequestData() {
        mArrayList.clear();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = "http://104.131.34.16:8080/spnet_api/fields.json";
        StringRequest JOR = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d(tag_string_req, response);
                        Log.d(TAG,response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("fields");
                            //JSONObject jsonObject7=jsonArray.getJSONObject(0);
                            //int ID=jsonObject7.getInt("id");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                //JSONArray jsonArray1=jsonObject1.getJSONArray("NewsRelease");
                                JSONObject jsonObject2 = jsonObject1.getJSONObject("Field");

                                String image = jsonObject2.getString("logo_image_url");
                                String title = jsonObject2.getString("name");
                                String phone = jsonObject2.getString("phone_line_1");

                                int id=Integer.parseInt(jsonObject2.getString("id"));

                                double longtitute=Double.parseDouble(jsonObject2.getString("longtitute"));
                                double lattitute=Double.parseDouble(jsonObject2.getString("lattitute"));

                                mArrayList.add(new ItemFieldFragment(title,image,phone,longtitute,lattitute,id));
                                Log.d("test", title);


                            }

                            //bestimage.add(image);


                            // progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("secret_key", "123");
                return headers;
            }
        };
        //queue.add(JOR);
       // AppController.getInstance().addToRequestQueue(JOR);
        MySingleton.getInstance(getActivity()).addToRequestQueue(JOR);

        //page=page+1;
    }

    public void addTextListener(){

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();

                final List<ItemFieldFragment> filteredList = new ArrayList<ItemFieldFragment>();

                for (int i = 0; i < mArrayList.size(); i++) {

                    final String text = mArrayList.get(i).getName_field().toLowerCase();
                    if (text.contains(query)) {


                        filteredList.add(new ItemFieldFragment(mArrayList.get(i).getName_field(),mArrayList.get(i).getMg_field(),mArrayList.get(i).getPhone(),mArrayList.get(i).getLongtitute(),mArrayList.get(i).getLattitute(),mArrayList.get(i).getId()));
                    }
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                mAdapter = new AdapterFieldFragment((ArrayList<ItemFieldFragment>) filteredList,getActivity());
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();  // data set changed
            }
        });
    }

    private ArrayList<ItemFieldSlide> slideFieldReques(ArrayList<ItemFieldSlide> slideField){
        ItemFieldSlide itemFieldSlide1=new ItemFieldSlide("https://i.ytimg.com/vi/tntOCGkgt98/maxresdefault.jpg","lela","hello","go","laday");
        slideField.add(itemFieldSlide1);

        ItemFieldSlide itemFieldSlide2=new ItemFieldSlide("https://images-na.ssl-images-amazon.com/images/G/01/img15/pet-products/small-tiles/30423_pets-products_january-site-flip_3-cathealth_short-tile_592x304._CB286975940_.jpg","hello","hello","go","laday");
        slideField.add(itemFieldSlide2);

        ItemFieldSlide itemFieldSlide3=new ItemFieldSlide("http://www.cats.org.uk/uploads/images/pages/photo_latest14.jpg","lela","hello","go","laday");
        slideField.add(itemFieldSlide3);
        return slideField;
    }


    }
