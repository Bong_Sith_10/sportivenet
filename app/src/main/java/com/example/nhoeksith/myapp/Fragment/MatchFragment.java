package com.example.nhoeksith.myapp.Fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nhoeksith.myapp.News.Item;
import com.example.nhoeksith.myapp.Match.ItemAdapterMatch;
import com.example.nhoeksith.myapp.R;

import java.util.ArrayList;

/**
 * Created by NHOEK Sith on 3/17/2017.
 */

public class MatchFragment extends android.support.v4.app.Fragment {
   // final private static int DIOLOG_REGISTER=1;
    private static final int DIALOG_REGISTER = 2;
    RecyclerView recyclerView;
    public MatchFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.match_fragment,container,false);
        recyclerView= (RecyclerView) view.findViewById(R.id.id_match_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager manager=new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(manager);
        ArrayList<Item> items=new ArrayList<Item>();
        ItemAdapterMatch itemAdapterMatch=new ItemAdapterMatch(items,getActivity().getApplicationContext());
        recyclerView.setAdapter(itemAdapterMatch);


        return view;
    }
}
