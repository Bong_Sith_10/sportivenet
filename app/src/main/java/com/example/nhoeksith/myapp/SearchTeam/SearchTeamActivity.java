package com.example.nhoeksith.myapp.SearchTeam;

import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.nhoeksith.myapp.R;

import java.util.ArrayList;

public class SearchTeamActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ArrayList<ItemTeam> mArrayList=new ArrayList<>();
    private AdapterTeam mAdapter;
    public String[] listTeam={"Bong chek FC","Bayern FC","Liverpool FC","Real Madrid FC","Barcelona FC","Juventus FC","Killer FC","Monoca FC","Chelcea FC","Man FC","Crystal Palace FC"};
    public String[] listCapitain={"Vy Darech","Bong Nang","Ly Kaka","Ny Sakada","Vong Buleng","Man Sela","Pi Pearum","Ly Unit","Set Raksa","My Boy","Bek Chaly"};
    public String[] listImageLogo={"https://s-media-cache-ak0.pinimg.com/736x/c8/65/5a/c8655a8435a98af243cb48b0eb0cf537.jpg",
                                    "https://s-media-cache-ak0.pinimg.com/736x/fa/e3/17/fae3179b999883b6377e238db841f4ea.jpg",
                                    "https://hdlogo.files.wordpress.com/2011/12/hiroshima-logo.png",
                                    "https://www.seeklogo.net/wp-content/uploads/2015/09/czech-republic-national-football-team-logo.png",
                                    "https://s-media-cache-ak0.pinimg.com/736x/a4/57/d5/a457d56cb453c6253752bd99749b479a.jpg",
                                    "https://hdlogo.files.wordpress.com/2011/12/avispa-fukuoka-logo.png",
                                    "http://www.newdesignfile.com/postpic/2010/03/lion-with-wings-logo_318541.png",
                                    "https://hdlogo.files.wordpress.com/2011/11/hjk-helsinki-logo.png",
                                    "https://hdlogo.files.wordpress.com/2013/07/adana-demirspor-hd-logo.png?w=640",
                                    "https://s-media-cache-ak0.pinimg.com/originals/70/74/f3/7074f32a8811a6644c5d34524831db6d.jpg",
                                    "https://thumb9.shutterstock.com/display_pic_with_logo/169918410/597293936/stock-vector-soccer-football-logo-emblem-designs-templates-on-a-dark-background-597293936.jpg"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_team);

        mRecyclerView = (RecyclerView) findViewById(R.id.id_rcv_team);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter=new AdapterTeam(mArrayList,this);
        mRecyclerView.setAdapter(mAdapter);

        getData();


    }

    public void getData(){
//        ItemTeam itemTeam=new ItemTeam("Barcelona Fc","Sith NHOEK");
//        mArrayList.add(itemTeam);
        String MgLogoTeam;


        ArrayList<ItemTeam> item;
        for (int i=0;i<listTeam.length;i++){
            String nameTeam= listTeam[i];
            String nameCapitain=listCapitain[i];
                MgLogoTeam=listImageLogo[i];

            mArrayList.add(new ItemTeam(nameTeam,nameCapitain,MgLogoTeam));


       }
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_itme,menu);
        MenuItem search=menu.findItem(R.id.sarch_view);
        SearchView searchView= (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    public void backSearchTeam(View view) {
        finish();
    }
}
