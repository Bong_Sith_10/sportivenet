package com.example.nhoeksith.myapp.News;

import android.app.ProgressDialog;
import android.content.Context;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nhoeksith.myapp.Activity.MainActivity;
import com.example.nhoeksith.myapp.ConvertDate.ConverDate;
import com.example.nhoeksith.myapp.News.Item;
import com.example.nhoeksith.myapp.News.ItemAdapter;
import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.help.MySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.nhoeksith.myapp.News.AppController.TAG;
import static com.example.nhoeksith.myapp.R.id.imageView;

/**
 * Created by NHOEK Sith on 3/17/2017.
 */

public class NewsFragment extends android.support.v4.app.Fragment {
    private static final String TAG = NewsFragment.class.getSimpleName();
    boolean a;

    ProgressDialog dialog;

     ArrayList<String> strings;
    ItemAdapter adapter;
    Item item;
    ArrayList<Item> data=new ArrayList<Item>();


    ViewFlipper viewFlipper;

    ImageView mg_slide1,mg_slide2,mg_slide3,mg_slide4;
    ImageView mg_point1_1,mg_point1_2,mg_point1_3,mg_point1,
            mg_point2,mg_point2_1,mg_point2_2,mg_point2_3,
            mg_point3,mg_point3_1,mg_point3_2,mg_point3_3,
            mg_point4,mg_point4_1,mg_point4_2,mg_point4_3;
    View point1,point2,point3,point4;

    View textSlide1,textSlide2,textSlide3,textSlide4;
    TextView tex_title,tex_title4,tex_title2,tex_title3,
              textSub_title,textSub_title4,textSub_title2,textSub_title3,
              textDate,textDate4,textDate2,textDate3;


    String[] imageFlipper={"http://www.bhmpics.com/thumbs/dimitri_payet_euro_2016-t3.jpg",
            "http://i4.mirror.co.uk/incoming/article9550397.ece/ALTERNATES/s615/Manchester-City-v-Burnley-Premier-League.jpg",
            "http://wallpapercave.com/wp/vP3y8yq.jpg",
            "https://images.cdn.fourfourtwo.com/sites/fourfourtwo.com/files/styles/image_landscape/public/most_expensive_players.png?itok=-aNFLP09"};



    RecyclerView recyclerView;
    ArrayList<String> images;
    ArrayList<String> lov;

    public NewsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.news_fragment,container,false);
        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Wait while loading...");
        dialog.isIndeterminate();
        dialog.setCancelable(true);

        //to do point slide
        point1=view.findViewById(R.id.id_point1);
        mg_point1= (ImageView) point1.findViewById(R.id.id_mg_point1); mg_point1.setImageResource(R.drawable.ic_point_big);
        mg_point1_1=(ImageView) point1.findViewById(R.id.id_mg_point2);mg_point1_1.setImageResource(R.drawable.ic_point_small);
        mg_point1_2=(ImageView) point1.findViewById(R.id.id_mg_point3);mg_point1_2.setImageResource(R.drawable.ic_point_small);
        mg_point1_3=(ImageView) point1.findViewById(R.id.id_mg_point4);mg_point1_3.setImageResource(R.drawable.ic_point_small);

        point2=view.findViewById(R.id.id_point2);
        mg_point2= (ImageView) point2.findViewById(R.id.id_mg_point1); mg_point2.setImageResource(R.drawable.ic_point_small);
        mg_point2_1=(ImageView) point2.findViewById(R.id.id_mg_point2);mg_point2_1.setImageResource(R.drawable.ic_point_big);
        mg_point2_2=(ImageView) point2.findViewById(R.id.id_mg_point3);mg_point2_2.setImageResource(R.drawable.ic_point_small);
        mg_point2_3=(ImageView) point2.findViewById(R.id.id_mg_point4);mg_point2_3.setImageResource(R.drawable.ic_point_small);

        point3=view.findViewById(R.id.id_point3);
        mg_point3= (ImageView) point3.findViewById(R.id.id_mg_point1); mg_point3.setImageResource(R.drawable.ic_point_small);
        mg_point3_1=(ImageView) point3.findViewById(R.id.id_mg_point2);mg_point3_1.setImageResource(R.drawable.ic_point_small);
        mg_point3_2=(ImageView) point3.findViewById(R.id.id_mg_point3);mg_point3_2.setImageResource(R.drawable.ic_point_big);
        mg_point3_3=(ImageView) point3.findViewById(R.id.id_mg_point4);mg_point3_3.setImageResource(R.drawable.ic_point_small);

        point4=view.findViewById(R.id.id_point4);
        mg_point4= (ImageView) point4.findViewById(R.id.id_mg_point1); mg_point4.setImageResource(R.drawable.ic_point_small);
        mg_point4_1=(ImageView) point4.findViewById(R.id.id_mg_point2);mg_point4_1.setImageResource(R.drawable.ic_point_small);
        mg_point4_2=(ImageView) point4.findViewById(R.id.id_mg_point3);mg_point4_2.setImageResource(R.drawable.ic_point_small);
        mg_point4_3=(ImageView) point4.findViewById(R.id.id_mg_point4);mg_point4_3.setImageResource(R.drawable.ic_point_big);


        textSlide1=view.findViewById(R.id.id_text_slide1);
        tex_title=(TextView) textSlide1.findViewById(R.id.id_text_rs);
        textSub_title=(TextView) textSlide1.findViewById(R.id.id_text_rst);
        textDate=(TextView)textSlide1.findViewById(R.id.id_text_date);

        textSlide2=view.findViewById(R.id.id_text_slide2);
        tex_title2=(TextView) textSlide2.findViewById(R.id.id_text_rs);
        textSub_title2=(TextView) textSlide2.findViewById(R.id.id_text_rst);
        textDate2=(TextView)textSlide2.findViewById(R.id.id_text_date);

        textSlide3=view.findViewById(R.id.id_text_slide3);
        tex_title3=(TextView) textSlide3.findViewById(R.id.id_text_rs);
        textSub_title3=(TextView) textSlide3.findViewById(R.id.id_text_rst);
        textDate3=(TextView)textSlide3.findViewById(R.id.id_text_date);

        textSlide4=view.findViewById(R.id.id_text_slide4);
        tex_title4=(TextView) textSlide4.findViewById(R.id.id_text_rs);
        textSub_title4=(TextView) textSlide4.findViewById(R.id.id_text_rst);
        textDate4=(TextView)textSlide4.findViewById(R.id.id_text_date);


        mg_slide1=(ImageView) view.findViewById(R.id.id_mg_slide1);
        mg_slide2=(ImageView) view.findViewById(R.id.id_mg_slide2);
        mg_slide3=(ImageView) view.findViewById(R.id.id_mg_slide3);
        mg_slide4=(ImageView)view.findViewById(R.id.id_mg_slide4);




        recyclerView=(RecyclerView) view.findViewById(R.id.id_new_recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager manager=new LinearLayoutManager(getActivity());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(manager);
        adapter=new ItemAdapter(data,getActivity());
        viewFlipper=(ViewFlipper)view.findViewById(R.id.id_slide_mg);

        recyclerView.setAdapter(adapter);

        RequestData();


        return view;
    }

    public void RequestData(){
        data.clear();
        String url = "http://104.131.34.16:8080/spnet_api/news_releases.json";
        StringRequest JOR = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG,response.toString());
                        try {

                            JSONObject jsonObject=new JSONObject(response);

                                JSONArray jsonArray=jsonObject.getJSONArray("news");
                                //JSONObject jsonObject7=jsonArray.getJSONObject(0);
                                //int ID=jsonObject7.getInt("id");
                                for(int i=0;i<jsonArray.length();i++){

                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    //JSONArray jsonArray1=jsonObject1.getJSONArray("NewsRelease");
                                    JSONObject jsonObject2=jsonObject1.getJSONObject("NewsRelease");

                                    String image=jsonObject2.getString("thumnail_image_url");
                                    String title=jsonObject2.getString("title");
                                    String date=jsonObject2.getString("created_date");
                                   // String subtitle=jsonObject2.getString("subtitle");
                                    String id=jsonObject2.getString("id");
                                    String url=jsonObject2.getString("url_content");
                                    ConverDate converDate=new ConverDate();
                                    String date_form= converDate.Conver(date);
                                    data.add(new Item(image,title,url,date_form));
                                    switch (i){
                                        case 0:tex_title.setText("Keiladaily");
                                            textSub_title.setText(title);
                                            textDate.setText(date_form);
                                            showImage(image,mg_slide1);
                                            break;
                                        case 1:tex_title2.setText("Keiladaily");
                                            textSub_title2.setText(title);
                                            textDate2.setText(date_form);
                                            showImage(image,mg_slide2);
                                            break;
                                        case 2:tex_title3.setText("Keiladaily");
                                            textSub_title3.setText(title);
                                            textDate3.setText(date_form);
                                            showImage(image,mg_slide3);
                                            break;
                                        case 3:tex_title4.setText("Keiladaily");
                                            textSub_title4.setText(title);
                                            textDate4.setText(date_form);
                                            showImage(image,mg_slide4);
                                            break;
                                    }
                                    Log.d("test",title);


                                }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("secret_key", "123");
                return headers;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(JOR);
        //AppController.getInstance().addToRequestQueue(JOR);

    }
    public void showImage(String img_url,ImageView imageView){
        Picasso.with(getActivity())
                .load(img_url)
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(imageView);

    }

    }






