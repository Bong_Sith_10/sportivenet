package com.example.nhoeksith.myapp.Fields;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.SearchField.ItemField;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class AdapterFieldFragment extends RecyclerView.Adapter<AdapterFieldFragment.ViewHolder>  {
    private ArrayList<ItemFieldFragment> mArrayListField;
    private ArrayList<ItemFieldFragment> mFilteredList;

    private Context mContext;

    public AdapterFieldFragment(ArrayList<ItemFieldFragment> arrayList, Context context) {
        this.mArrayListField=arrayList;
        this.mFilteredList=arrayList;

        this.mContext=context;
    }

    @Override
    public AdapterFieldFragment.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_search_field,parent,false);

        return new ViewHolder(view,mArrayListField,mContext);
    }

    @Override
    public void onBindViewHolder(AdapterFieldFragment.ViewHolder holder, int i) {
        ItemFieldFragment itemFieldFragment=mArrayListField.get(i);
        holder.tv_name_field.setText(itemFieldFragment.getName_field());
        holder.tv_phone_field.setText(itemFieldFragment.getPhone());
        Picasso.with(mContext)
                .load(mArrayListField.get(i).getMg_field())
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.mg_logo_field);
    }

    @Override
    public int getItemCount() {
        return mArrayListField.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_name_field,tv_phone_field;
        private ImageView mg_logo_field;
        private CardView cv;
        ArrayList<ItemFieldFragment> arrayList=new ArrayList<ItemFieldFragment>();
        Context mContext;

        public ViewHolder(View view,ArrayList<ItemFieldFragment> arrayList,Context context) {

            super(view);

            this.arrayList=arrayList;
            this.mContext=context;
            cv= (CardView) view.findViewById(R.id.id_cv_searchField);
            tv_name_field = (TextView) view.findViewById(R.id.id_tv_FieldName);
            mg_logo_field = (ImageView) view.findViewById(R.id.id_field_logo);
            tv_phone_field=(TextView)view.findViewById(R.id.id_tv_phone_field);

            cv.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position=getAdapterPosition();
            ItemFieldFragment itemFieldFragment=this.arrayList.get(position);
            Intent intent=new Intent(mContext,DetailFieldActivity.class);
            intent.putExtra("LogoField",itemFieldFragment.getMg_field());
            intent.putExtra("NameField",itemFieldFragment.getName_field());
            intent.putExtra("longt",itemFieldFragment.getLongtitute());
            intent.putExtra("latt",itemFieldFragment.getLattitute());
            intent.putExtra("id",itemFieldFragment.getId());
            mContext.startActivity(intent);
        }
    }
}
