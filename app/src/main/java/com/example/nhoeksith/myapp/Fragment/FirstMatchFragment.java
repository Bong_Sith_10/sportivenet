package com.example.nhoeksith.myapp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.nhoeksith.myapp.Activity.LoginActivity;
import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.help.SessionManager;

/**
 * Created by NHOEK Sith on 5/19/2017.
 */

public class FirstMatchFragment extends android.support.v4.app.Fragment{
    SessionManager session;
    Button bnt_lgin;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        View view=inflater.inflate(R.layout.first_match_fragement,container,false);
        bnt_lgin= (Button) view.findViewById(R.id.id_log_sign);

        bnt_lgin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return view;
    }
}
