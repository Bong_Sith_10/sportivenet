package com.example.nhoeksith.myapp.SearchField;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.SearchTeam.ItemTeam;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class AdapterField extends RecyclerView.Adapter<AdapterField.ViewHolder>  {
    private ArrayList<ItemField> mArrayListField;
    private ArrayList<ItemField> mFilteredList;

    private Context mContext;

    public AdapterField(ArrayList<ItemField> arrayList, Context context) {
        this.mArrayListField=arrayList;
        this.mFilteredList=arrayList;

        this.mContext=context;
    }

    @Override
    public AdapterField.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_search_field,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterField.ViewHolder holder, int i) {
        ItemField itemTeam=mArrayListField.get(i);
        holder.tv_name_field.setText(itemTeam.getName_field());
        holder.tv_phone_field.setText(itemTeam.getField_phone());
        Picasso.with(mContext)
                .load(mArrayListField.get(i).getMg_field())
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.mg_logo_field);
    }

    @Override
    public int getItemCount() {
        return mArrayListField.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name_field,tv_phone_field;
        private ImageView mg_logo_field;
        private CardView cv;

        public ViewHolder(View view) {
            super(view);
           // cv= (CardView) view.findViewById(R.id.id_cv_searchTeam);
            tv_phone_field=(TextView)view.findViewById(R.id.id_tv_phone_field);
            tv_name_field = (TextView) view.findViewById(R.id.id_tv_FieldName);
            mg_logo_field = (ImageView) view.findViewById(R.id.id_field_logo);


        }
    }
}
