package com.example.nhoeksith.myapp.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.nhoeksith.myapp.Fragment.SentFragment;
import com.example.nhoeksith.myapp.Fragment.TabFragment;
import com.example.nhoeksith.myapp.Language.LanguageActivity;
import com.example.nhoeksith.myapp.Navigation.AdapterTeamNav;
import com.example.nhoeksith.myapp.Navigation.ItemTeamNav;
import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.help.Model;
import com.example.nhoeksith.myapp.help.SessionManager;


import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {
    final private static int DIALOG_LOGIN = 1;
    final private static int DIALOG_REGISTER = 2;
    private  RecyclerView recyclerView;
    private ArrayList<ItemTeamNav> arrayList=new ArrayList<>();
    private AdapterTeamNav adapterTeamNav;
    RelativeLayout layout;

    public String[] listTeam={"Bong chek FC"};
    public String[] listImageLogo={"https://s-media-cache-ak0.pinimg.com/736x/c8/65/5a/c8655a8435a98af243cb48b0eb0cf537.jpg"};

    private int[] tableIcon = {R.drawable.ic_new_24dp, R.drawable.ic_match_24dp, R.drawable.ic_field_24dp};
    RelativeLayout relativeLayout;

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    SessionManager sessionManager;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        /**
         *Setup the DrawerLayout and NavigationView
         */
        sessionManager=new SessionManager(getApplicationContext());

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);


//        recyclerView= (RecyclerView) findViewById(R.id.id_nav_rc);
//        recyclerView.setHasFixedSize(true);
//        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,true);
//        recyclerView.setLayoutManager(layoutManager);
//        adapterTeamNav=new AdapterTeamNav(arrayList,this);
//        recyclerView.setAdapter(adapterTeamNav);
//        getData();



        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();


        //navHeader=mNavigationView.getHeaderView(0);
       // layout=(RelativeLayout) findViewById(R.id.id_nav_layout);
        /**
         * Setup click events on the Navigation View Items.
         */

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();


                if (menuItem.getItemId() == R.id.nav_item_sent) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerView, new SentFragment()).commit();

                }

                if (menuItem.getItemId() == R.id.nav_item_inbox) {
                    FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                    xfragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();
                }

                return false;
            }

        });

        /**
         * Setup Drawer Toggle of the Toolbar
         */

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();


    }

    public void getData(){
//        ItemTeam itemTeam=new ItemTeam("Barcelona Fc","Sith NHOEK");
//        mArrayList.add(itemTeam);
        String MgLogoTeam;


        ArrayList<ItemTeamNav> item;
        for (int i=0;i<10;i++){
            String nameTeam= listTeam[0];
            MgLogoTeam=listImageLogo[0];

            arrayList.add(new ItemTeamNav(nameTeam,MgLogoTeam));


        }
        adapterTeamNav.notifyDataSetChanged();

    }


    public void OnClickLanguage(View view) {
        Intent intent=new Intent(this, LanguageActivity.class);
        startActivity(intent);
    }

    public void OnClickLoginDialog(View view) {

        Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void OnClickLogout(View view) {
        sessionManager.setLogin(false);
        Intent intent=new Intent(HomeActivity.this,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();


    }
}







