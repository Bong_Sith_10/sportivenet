package com.example.nhoeksith.myapp.guide;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.nhoeksith.myapp.Activity.SignUpActivity;

/**
 * Created by NHOEK Sith on 3/27/2017.
 */

public class PagerAdapter extends FragmentPagerAdapter {
    SignUpActivity signUpActivity=new SignUpActivity();
    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int arg0) {
        switch (arg0){
            case 0:
                return new FragmentOne();
            case 1:
                return new FragmentTwo();
            case 2:
                return new FragmentThree();
            default:
                break;

        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
