package com.example.nhoeksith.myapp.CreateMatch;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by NHOEK Sith on 4/10/2017.
 */

public class TapCreatMatchAdapter extends FragmentPagerAdapter {
    int NumOfTab;
    public TapCreatMatchAdapter(FragmentManager fm,int NumOftabs) {
        super(fm);
        this.NumOfTab=NumOftabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                //show match team
                return new TeamMatchFragment();
            case 1:
                return new GroupeMatchFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return NumOfTab;
    }
}
