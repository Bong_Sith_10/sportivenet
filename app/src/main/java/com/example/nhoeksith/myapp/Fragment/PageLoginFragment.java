package com.example.nhoeksith.myapp.Fragment;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.example.nhoeksith.myapp.ImageView.ImageBackground;
import com.example.nhoeksith.myapp.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by NHOEK Sith on 3/15/2017.
 */

public class PageLoginFragment extends android.app.Fragment {
    ViewFlipper viewFlipper;
    String[] imageFlipper={"http://www.bhmpics.com/thumbs/dimitri_payet_euro_2016-t3.jpg",
                            "http://www.may2015.com/wp-content/uploads/2015/04/Screen-Shot-2015-04-23-at-12.31.11.png",
                            "http://wallpapercave.com/wp/vP3y8yq.jpg"};
    public PageLoginFragment() {
    }

    TextView textView;
    //ImageBackground imageBackground;
    //RelativeLayout relativeLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.pagelogin_layout,container,false);
        //imageBackground.getBitmapFromURL("http://www.hdiphonewallpapers.us/phone-wallpapers/iphone-6s-wallpaper-1-720x1280/iphone-6s-wallpaper-HD-468pv5gi-720x1280.jpg");
        //relativeLayout= (RelativeLayout) view.findViewById(R.id.id_layout_pagelogin);
       // Drawable drawable=new BitmapDrawable(String.valueOf(imageBackground));
        //relativeLayout.setBackgroundDrawable(drawable);
        textView= (TextView) view.findViewById(R.id.id_ap_title);
        Shader shader=new LinearGradient(0,0,0,textView.getTextSize(),
                Color.RED,Color.BLUE, Shader.TileMode.CLAMP);
       // textView.getPaint().setShader(shader);

//       ViewPager mViewPager=(ViewPager) view.findViewById(R.id.slide_image);
//        ImageAdapter imageAdapter=new ImageAdapter(getActivity());
//       mViewPager.setAdapter(imageAdapter);

        viewFlipper= (ViewFlipper) view.findViewById(R.id.id_Image_Slider);
        //viewFlipper.setFlipInterval(1000);

        for (int i=0; i<imageFlipper.length;i++){
            ImageView imageView=new ImageView(getActivity());
            Picasso.with(getActivity())
            .load(imageFlipper[i])
            .into(imageView);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            viewFlipper.addView(imageView,i);
            viewFlipper.setAutoStart(true);
            viewFlipper.setFlipInterval(1000);
            viewFlipper.showNext();
            viewFlipper.startFlipping();
           // viewFlipper.setOutAnimation(anim);

            viewFlipper.setInAnimation(getActivity(), R.anim.slide_in_right);
            viewFlipper.setOutAnimation(getActivity(),R.anim.slide_out_left);

        }

        return view;
    }




}
