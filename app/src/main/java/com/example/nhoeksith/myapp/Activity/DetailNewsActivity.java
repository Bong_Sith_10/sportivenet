package com.example.nhoeksith.myapp.Activity;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.nhoeksith.myapp.R;

public class DetailNewsActivity extends AppCompatActivity {
    public AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);
        appBarLayout = (AppBarLayout) findViewById(R.id.id_appbar_dtNews);
    }

    public void BackToDetail(View view) {
        Intent intent=new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}
