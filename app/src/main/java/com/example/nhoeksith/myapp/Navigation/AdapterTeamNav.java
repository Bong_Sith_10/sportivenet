package com.example.nhoeksith.myapp.Navigation;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.SearchTeam.ItemTeam;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class AdapterTeamNav extends RecyclerView.Adapter<AdapterTeamNav.ViewHolder> {
    private ArrayList<ItemTeamNav> mArrayListField;
    private ArrayList<ItemTeamNav> mFilteredList;

    private Context mContext;

    public AdapterTeamNav(ArrayList<ItemTeamNav> arrayList, Context context) {
        this.mArrayListField=arrayList;
        this.mFilteredList=arrayList;

        this.mContext=context;
    }

    @Override
    public AdapterTeamNav.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_nav_team,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterTeamNav.ViewHolder holder, int i) {
        ItemTeamNav itemTeam=mArrayListField.get(i);
        holder.tv_name_Team.setText(itemTeam.getName_team());
        Picasso.with(mContext)
                .load(mArrayListField.get(i).getMglogoTeam())
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.MglogoTeam);

    }

    @Override
    public int getItemCount() {
        return mArrayListField.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name_Team, tv_name_Capitain;
        private CardView cv;
        private CircleImageView MglogoTeam;

        public ViewHolder(View view) {
            super(view);
           // cv= (CardView) view.findViewById(R.id.id_cv_searchTeam);
            tv_name_Team = (TextView) view.findViewById(R.id.id_nameTeamNav);
            MglogoTeam=(CircleImageView) view.findViewById(R.id.id_navMgTeam);


        }
    }
}
