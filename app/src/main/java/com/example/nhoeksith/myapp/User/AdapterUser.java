package com.example.nhoeksith.myapp.User;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.SearchField.ItemField;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder>  {
    private ArrayList<ItemUser> mArrayListField;
    private ArrayList<ItemUser> mFilteredList;

    private Context mContext;

    public AdapterUser(ArrayList<ItemUser> arrayList, Context context) {
        this.mArrayListField=arrayList;
        this.mFilteredList=arrayList;

        this.mContext=context;
    }

    @Override
    public AdapterUser.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_search_user,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterUser.ViewHolder holder, int i) {
        ItemUser itemUser=mArrayListField.get(i);
        holder.tv_name_user.setText(itemUser.getName_user());
        Picasso.with(mContext)
                .load(mArrayListField.get(i).getMg_user())
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.mg_user);
    }

    @Override
    public int getItemCount() {
        return mArrayListField.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name_user;
        private ImageView mg_user;
        private CardView cv;

        public ViewHolder(View view) {
            super(view);
           // cv= (CardView) view.findViewById(R.id.id_cv_searchTeam);
            tv_name_user = (TextView) view.findViewById(R.id.id_tvName_user);
            mg_user = (ImageView) view.findViewById(R.id.id_mg_searchUser);

        }
    }
}
