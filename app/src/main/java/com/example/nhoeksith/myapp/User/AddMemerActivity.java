package com.example.nhoeksith.myapp.User;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nhoeksith.myapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddMemerActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private ArrayList<ItemUser> mArrayList=new ArrayList<>();
    private AdapterUser mAdapter;
    public String[] listUserName={"Bong Ka","Vutha Ka","Ly Lay","See You","Ly Unit","Tha Kakana","Romdul Smos Snae","Bora Chan","Fracois Ke","Ma Mail","Kmeng Touch Nice"};
    public String[] listMgUser={"http://2.bp.blogspot.com/-z6vG8XPUh4Q/U617sotUQZI/AAAAAAAALmE/_kilY4WRcv4/s1600/cute-and-beautiful-girls-wallpapers-005.jpg",
                                    "https://static.independent.co.uk/s3fs-public/thumbnails/image/2016/01/10/13/gareth-bale-23324.jpg",
                                    "http://images.performgroup.com/di/library/Goal_France/dc/f7/edinson-cavani-paris-sg-bordeaux-ligue-1-01102016_170dwz0ll5gok11s8ukvr5ezf9.jpg?t=-2131989609&w=620&h=430",
                                    "http://i3.liverpoolecho.co.uk/incoming/article11129524.ece/ALTERNATES/s615/JS86406148.jpg"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_memer);


        recyclerView=(RecyclerView)findViewById(R.id.id_rv_search_member);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        mAdapter=new AdapterUser(mArrayList,this);
        recyclerView.setAdapter(mAdapter);
        RequestData();

      //  getData();
    }

//    public void getData() {
////        ItemTeam itemTeam=new ItemTeam("Barcelona Fc","Sith NHOEK");
////        mArrayList.add(itemTeam);
//
//
//        ArrayList<ItemUser> item;
//        String  mgUser;
//        for (int i = 0; i < listUserName.length; i++) {
//            String nameUser = listUserName[i];
//
//            if (i>3){
//                mgUser=listMgUser[3];
//
//            }else {
//                mgUser = listMgUser[i];
//            }
//            mArrayList.add(new ItemUser(nameUser, mgUser));
//
//
//        }
//        mAdapter.notifyDataSetChanged();
//    }


    public void BackSearchUser(View view) {
        finish();
    }

    public void RequestData() {
        // progressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://104.131.34.16:8080/spnet_api/users.json";
        StringRequest JOR = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d(TAG, response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray jsonArray = jsonObject.getJSONArray("users");
                            //JSONObject jsonObject7=jsonArray.getJSONObject(0);
                            //int ID=jsonObject7.getInt("id");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                //JSONArray jsonArray1=jsonObject1.getJSONArray("NewsRelease");
                                JSONObject jsonObject2 = jsonObject1.getJSONObject("User");

                                String image = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg";
                                String title = jsonObject2.getString("username");
                               // String phone = jsonObject2.getString("phone_line_1");
                                // String subtitle=jsonObject2.getString("subtitle");
                                String id = jsonObject2.getString("id");
                                //String url = jsonObject2.getString("url_content");
                                //bestimage.add(image);
                                //bestimage.add(image);
//                                ConverDate converDate = new ConverDate();
//                                String date_form = converDate.Conver(date);

                                mArrayList.add(new ItemUser(title,image));
                                Log.d("test", title);


                            }
                            //bestimage.add(image);


                            // progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("secret_key", "123");
                return headers;
            }
        };
        queue.add(JOR);

        //page=page+1;
    }

}
