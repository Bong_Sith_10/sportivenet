package com.example.nhoeksith.myapp.Activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.guide.QuideActivity;

public class PhoneNumber extends AppCompatActivity {
    private EditText inputPhone;
    private TextInputLayout inputLayoutPhone;
    Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number);

        inputPhone= (EditText) findViewById(R.id.input_phone);
        inputLayoutPhone= (TextInputLayout) findViewById(R.id.input_phone_layout);
        btnNext= (Button) findViewById(R.id.bnt_register);

//        btnNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent=new Intent(getApplication(),InputCode.class);
//                startActivity(intent);
//
//
//            }
//        });

    }

    public void InputPhone(View view) {
        Intent intent=new Intent(this,InputCode.class);
        startActivity(intent);
        finish();
    }

    public void BackToFullName(View view) {
        Intent intent=new Intent(this,QuideActivity.class);
        startActivity(intent);
        finish();
    }
}
