package com.example.nhoeksith.myapp.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.help.MySingleton;

public class SignUpActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private EditText inputName, inputEmail, inputPassword;
    private TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutPassword;
    private Button btnSignUp;
    Intent intent;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Button buttonReg2;
    EditText txtUsername, txtPassword, txtEmail;
    MySingleton session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
       intent =new Intent(this,LoginActivity.class);
       // getNumber=new GetNumber(1);
        txtUsername=(EditText)findViewById(R.id.id_full_name);
        txtEmail=(EditText)findViewById(R.id.id_username);
        txtPassword=(EditText)findViewById(R.id.id_password);



        sharedPreferences = getApplicationContext().getSharedPreferences("Reg", 0);
        // get editor to edit in file
        editor = sharedPreferences.edit();

    }

    public void OnClickRegister(View view) {
        String name = txtUsername.getText().toString();
        String email = txtEmail.getText().toString();
        String pass = txtPassword.getText().toString();

        if(txtUsername.getText().length()<=0){
            Toast.makeText(SignUpActivity.this, "Enter name", Toast.LENGTH_SHORT).show();
        }
        else if( txtEmail.getText().length()<=0){
            Toast.makeText(SignUpActivity.this, "Enter email", Toast.LENGTH_SHORT).show();
        }
        else if( txtPassword.getText().length()<=0){
            Toast.makeText(SignUpActivity.this, "Enter password", Toast.LENGTH_SHORT).show();
        }
        else{

            // as now we have information in string. Lets stored them with the help of editor
            editor.putString("Name", name);
            editor.putString("Email",email);
            editor.putString("txtPassword",pass);
            editor.commit();}   // commit the values

        // after saving the value open next activity
        Intent ob = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(ob);
        finish();


    }

    public void OnClickCancelRg(View view) {
        startActivity(intent);
        finish();

    }
}
