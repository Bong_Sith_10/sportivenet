package com.example.nhoeksith.myapp.Fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nhoeksith.myapp.News.Item;
import com.example.nhoeksith.myapp.News.NewsFragment;
import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.help.Model;
import com.example.nhoeksith.myapp.help.SessionManager;

import java.util.ArrayList;

/**
 * Created by NHOEK Sith on 3/20/2017.
 */

public class TabFragment extends android.support.v4.app.Fragment {
    ArrayList<Item> items=new ArrayList<>();
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 3 ;
    SessionManager sessionManager;

    private int[] tableIcon={R.drawable.ic_new_24dp,R.drawable.ic_match_24dp,R.drawable.ic_field_24dp};

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sessionManager=new SessionManager(getActivity());
        /**
         *Inflate tab_layout and setup Views.
         */
        View x =  inflater.inflate(R.layout.tab_layout,null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);


        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));


        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.getTabAt(0).setIcon(tableIcon[0]);
                tabLayout.getTabAt(1).setIcon(tableIcon[1]);
                tabLayout.getTabAt(2).setIcon(tableIcon[2]);
            }
        });

        return x;

    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public android.support.v4.app.Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new NewsFragment();
                case 1 :
                    if (sessionManager.isLoggedIn()) {
                        return new MatchFragment();
                    }else {
                        return new FirstMatchFragment();
                    }
                case 2 : return new FieldFragment();
            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :

                    return "News";
                case 1 :
                    return "Matching";
                case 2 :
                    return "F.Field";
            }
            return null;
        }
    }


}
