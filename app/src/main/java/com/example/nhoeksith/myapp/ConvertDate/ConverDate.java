package com.example.nhoeksith.myapp.ConvertDate;

import android.net.ParseException;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by NHOEK Sith on 5/16/2017.
 */

public class ConverDate {
    public String Conver(String day){


//        String year=day.substring(0,4);
//        int cc=Integer.parseInt(day.substring(0,2));
//        int yy=Integer.parseInt(day.substring(2,4));
//        int month=Integer.parseInt(day.substring(5,7));
//        int days=Integer.parseInt(day.substring(8,10));
//        String week[]={"អាទិត្យ","ច័ន្ទ","អង្គារ","ពុធ"," ព្រហស្បត៏","សុក្រ","សៅរ៏"};
//        String months[]={"","មការា","កុម្ភះ","មិនា","មេសា","ឧសភា","មិថុនា","កក្តដា","សីហា","កញ្ញា","តុលា","វិច្ឆិកា","ធ្នូ"};
//        int i=(((cc/4)-2*cc-1)+(yy*5/4)+((month+1)*26/10)+days) % 7;
//        String weeks=week[i];
//        return (weeks+" "+" "+days+" "+months[month]+","+year);

        SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date objectCreatedDate = null;
        Date currentDate = new Date();
        try
        {objectCreatedDate = format.parse(day);}
        catch (ParseException e)
        {
            Log.e(TAG, e.getMessage());} catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        format=new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        return String.valueOf(format.format(objectCreatedDate));

    }

}
