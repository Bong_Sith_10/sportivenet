package com.example.nhoeksith.myapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.example.nhoeksith.myapp.R;
import com.example.nhoeksith.myapp.help.SessionManager;
import com.example.nhoeksith.myapp.help.MySingleton;

public class LoginActivity extends AppCompatActivity {
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    StringRequest request;
    RequestQueue requestQueue;


    private static final String TAG = LoginActivity.class.getSimpleName();
    private Button btnLogin,btnCanCel;
    private Button btnLinkToRegister;
    private Button btFbLogin;
    private EditText inputUsername;
    private EditText inputPassword;

    private static final String PREFER_NAME = "Reg";
    EditText txtUsername, txtPassword;

    MySingleton session;
    SessionManager sessionManager;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin=(Button)findViewById(R.id.btn_login);
        btFbLogin=(Button)findViewById(R.id.id_fb_login);
        btnLinkToRegister=(Button) findViewById(R.id.id_sign_up);
        txtUsername =(EditText)findViewById(R.id.txt_name);
        txtPassword=(EditText)findViewById(R.id.password);
        btnCanCel=(Button)findViewById(R.id.btn_cancel);


        sessionManager=new SessionManager(getApplicationContext());

        sharedPreferences = getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);


    }



    public void OnClickSignUp(View view) {
        Intent intent=new Intent(LoginActivity.this,SignUpActivity.class);
        startActivity(intent);
        finish();


    }

    public void onClickLoginFb(View view) {
    }

    public void OnClickCancel(View view) {
        Intent intent=new Intent(this,HomeActivity.class);
        startActivity(intent);
        finish();

    }


    public void OnClickLogin(View view) {
        sessionManager.setLogin(true);
        Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();

    }
}
