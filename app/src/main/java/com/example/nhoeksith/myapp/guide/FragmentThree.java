package com.example.nhoeksith.myapp.guide;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nhoeksith.myapp.Activity.SignUpActivity;
import com.example.nhoeksith.myapp.R;

/**
 * Created by NHOEK Sith on 3/27/2017.
 */

public class FragmentThree extends android.support.v4.app.Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_layout_guide3,container,false);
        return view;
    }

}
