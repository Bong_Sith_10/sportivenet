package com.example.nhoeksith.myapp.SearchTeam;

import java.io.Serializable;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class ItemTeam implements Serializable {
    public String mglogoTeam;
    public String name_field;
    public String name_capitain;
    public String api;

    public ItemTeam(String name_field, String name_capitain,String mglogoTeam) {
        this.name_field = name_field;
        this.name_capitain = name_capitain;
        this.mglogoTeam=mglogoTeam;
    }

    public String getMglogoTeam() {
        return mglogoTeam;
    }

    public void setMglogoTeam(String mglogoTeam) {
        this.mglogoTeam = mglogoTeam;
    }

    public String getName_field() {
        return name_field;
    }

    public void setName_field(String name_field) {
        this.name_field = name_field;
    }

    public String getName_capitain() {
        return name_capitain;
    }

    public void setName_capitain(String name_capitain) {
        this.name_capitain = name_capitain;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }
}
