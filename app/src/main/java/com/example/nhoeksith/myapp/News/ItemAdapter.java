package com.example.nhoeksith.myapp.News;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nhoeksith.myapp.Activity.DetailNewsActivity;
import com.example.nhoeksith.myapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NHOEK Sith on 4/3/2017.
 */

public class ItemAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private ArrayList<Item> mItemList;
     Context mContext;

   // final int VIEW_0=0;
    //final int VIEW_1=1;

    public ItemAdapter(ArrayList<Item> itemList, Context context) {
        this.mItemList=itemList;
        this.mContext=context;
    }

//    @Override
//    public int getItemViewType(int position) {
//        if (position==0){
//            return null;
//        }else {
//            return VIEW_1;
//        }
//
//    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_view2,parent,false);
//        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
//        View view;
//        RecyclerView.ViewHolder holder=null;
//


//        if (viewType==0){
//            view=inflater.inflate(R.layout.item_card_view1,parent,false);
//            holder=new ItemViewHolder0(view);

    //    }
//        if (viewType==1){
//            view = inflater.inflate(R.layout.item_card_view2, parent, false);
//            holder=new ItemViewHolder1(view);
//        }
//        return holder;
        return new ItemViewHolder1(view,mItemList,mContext);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
       // Item item=mItemList.get(position);
        ItemViewHolder1 holder1= (ItemViewHolder1) holder;
        holder1.title.setText(mItemList.get(position).getTitle());
        holder1.created_at.setText(mItemList.get(position).getCreated_at());
        //holder1.subtitle.setText(mItemList.get(position).getUrl());
        Picasso.with(mContext)
                .load(mItemList.get(position).getImage())
                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
                .error(android.R.drawable.stat_notify_error)
                .into(holder1.ivImage);


//        if (holder instanceof ItemViewHolder0){
//            ItemViewHolder0 holder0= (ItemViewHolder0) holder;
            //holder1.tvDesc.setText(mItemList.get(position-1).getTittle());
//            Picasso.with(mContext)
//                    .load(mItemList.get(position).image1)
//                    .placeholder(android.R.drawable.ic_menu_upload_you_tube)
//                    .error(android.R.drawable.stat_notify_error)
//                    .into(holder0.ivImage);
//        }
//        if (holder instanceof ItemViewHolder1){
//            ItemViewHolder1 holder1= (ItemViewHolder1) holder;
//            holder1.tvDesc.setText(mItemList.get(position-1).title);
//            Picasso.with(mContext)
//                    .load(mItemList.get(position-1).image)
//                    .placeholder(android.R.drawable.ic_menu_upload_you_tube)
//                    .error(android.R.drawable.stat_notify_error)
//                    .into(holder1.ivImage);
//
//        }
//        Item item=mItemList.get(position);
//        Picasso.with(mContext)
//                .load(item.image)
//                .placeholder(android.R.drawable.ic_menu_upload_you_tube)
//                .error(android.R.drawable.stat_notify_error)
//                .into();


    }

    @Override
    public int getItemCount() {
        if (mItemList!=null){
            return mItemList.size();
        }
        return 0;
    }

//    public static class ItemViewHolder0 extends RecyclerView.ViewHolder implements View.OnClickListener {
//        public CardView cvItem;
//        public ImageView ivImage;
//        Context context;
//
//        public ItemViewHolder0(View itemView) {
//            super(itemView);
//            cvItem=(CardView) itemView.findViewById(R.id.card_view_id);
//            ivImage=(ImageView)itemView.findViewById(R.id.im_new_id);
//            itemView.setOnClickListener(this);
//        }
//
//
//        @Override
//        public void onClick(View view) {
//            context=itemView.getContext();
//            Intent intent=new Intent(context,DetailNewsActivity.class);
//            context.startActivity(intent);
//        }
//    }
    private static class ItemViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CardView cvItem;
        private ImageView ivImage;
        private TextView title,subtitle,created_at;
    ArrayList<Item> itemNews=new ArrayList<Item>();
    Context context;


        private ItemViewHolder1(View itemView ,ArrayList<Item> itemNews,Context context) {
            super(itemView);
            this.itemNews=itemNews;
            this.context=context;


            cvItem=(CardView) itemView.findViewById(R.id.card_view2_id);
            ivImage=(ImageView)itemView.findViewById(R.id.im2_new_id);
            title=(TextView) itemView.findViewById(R.id.id_text_card2_1);
            subtitle=(TextView) itemView.findViewById(R.id.id_text_card2);
            created_at=(TextView) itemView.findViewById(R.id.id_cardView2_date);
            itemView.setOnClickListener(this);


        }


        @Override
        public void onClick(View view) {
            int positon=getAdapterPosition();
             //context=itemView.getContext();
            Item item=this.itemNews.get(positon);
           // Intent intent=new Intent(context,DetailNewsActivity.class);
            Intent intent=new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.setData(Uri.parse(item.getUrl()));
            context.startActivity(intent);
        }
    }
}
