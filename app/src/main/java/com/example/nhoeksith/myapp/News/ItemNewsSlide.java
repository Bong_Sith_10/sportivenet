package com.example.nhoeksith.myapp.News;

import java.io.Serializable;

/**
 * Created by NHOEK Sith on 5/18/2017.
 */

public class ItemNewsSlide implements Serializable {
    String mg_new_slide;

    public ItemNewsSlide(String mg_new_slide) {
        this.mg_new_slide = mg_new_slide;
    }

    public String getMg_new_slide() {
        return mg_new_slide;
    }

    public void setMg_new_slide(String mg_new_slide) {
        this.mg_new_slide = mg_new_slide;
    }
}
