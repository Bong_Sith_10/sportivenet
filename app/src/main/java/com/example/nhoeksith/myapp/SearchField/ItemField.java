package com.example.nhoeksith.myapp.SearchField;

import java.io.Serializable;

/**
 * Created by NHOEK Sith on 4/21/2017.
 */

public class ItemField implements Serializable {

    private String name_field;
    private String mg_field;
    private String field_phone;
    public String api;

    public ItemField(String name_field, String mg_field,String field_phone) {
        this.field_phone=field_phone;
        this.name_field = name_field;
        this.mg_field = mg_field;
        //this.api = api;
    }

    public String getField_phone() {
        return field_phone;
    }

    public void setField_phone(String field_phone) {
        this.field_phone = field_phone;
    }

    public String getName_field() {
        return name_field;
    }

    public void setName_field(String name_field) {
        this.name_field = name_field;
    }

    public String getMg_field() {
        return mg_field;
    }

    public void setMg_field(String mg_field) {
        this.mg_field = mg_field;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }
}
